<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(4)->create();
        // \App\Models\admin::factory(4)->create();
        // \App\Models\category::factory(4)->create();
        \App\Models\course::factory(5)->create();
    }
}
