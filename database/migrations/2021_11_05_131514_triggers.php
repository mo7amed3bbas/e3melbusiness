<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Triggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::unprepared("
            DROP TRIGGER IF EXISTS `courses_CountIns`;  CREATE TRIGGER `courses_CountIns`   AFTER INSERT ON `courses` FOR EACH ROW Update `categories` SET `categories`.`countCourses`  = `categories`.`countCourses` +1   WHERE NEW.category_id = `categories`.`id` ;
            DROP TRIGGER IF EXISTS `courses_CountUpdt`; CREATE TRIGGER `courses_CountUpdt`  AFTER UPDATE ON `courses` FOR EACH ROW Update `categories` SET `categories`.`countCourses`  = `categories`.`countCourses` +1   WHERE NEW.category_id = `categories`.`id` ;
            DROP TRIGGER IF EXISTS `courses_CountUpdt2`;CREATE TRIGGER `courses_CountUpdt2` AFTER UPDATE ON `courses` FOR EACH ROW Update `categories` SET `categories`.`countCourses`  = `categories`.`countCourses` -1   WHERE old.category_id = `categories`.`id` ;
            DROP TRIGGER IF EXISTS `courses_CountDel`;  CREATE TRIGGER `courses_CountDel`   AFTER DELETE ON `courses` FOR EACH ROW Update `categories` SET `categories`.`countCourses`  = `categories`.`countCourses` -1   WHERE old.category_id = `categories`.`id` ;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
