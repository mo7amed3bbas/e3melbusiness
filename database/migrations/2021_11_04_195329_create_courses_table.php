<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)                                 ->nullable();
            $table->text('description')                                 ->nullable();
            $table->enum('level', ['beginner', 'intermediate' ,'high'])                     ->default('beginner');
            $table->integer('votes')                                    ->nullable()        ->default(0);
            $table->integer('hours')                                    ->nullable()        ->default(0);
            $table->bigInteger('category_id')                           ->unsigned();
            $table->enum('status', ['Active', 'inActive'])              ->default('Active');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')
            ->references('id')->on('categories')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
