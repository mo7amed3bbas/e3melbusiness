<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class courseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          => $this->faker->name(),
            'description'   => Str::random(20),
            'category_id'   => rand(1,2),
            'votes'         => rand(1,20),
            'hours'         => rand(30,120),
            'created_at'    => now(), // password
            'updated_at'    => now(), // password
        ];
    }
}
