<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('login'                  ,       '\App\Http\Controllers\Auth\AdminLoginController@login')  ->name('dashboard.login');
Route::post('login'                 ,       '\App\Http\Controllers\Auth\AdminLoginController@SignIn') ->name('dashboard.SignIn');
Route::get('logout'                 ,       '\App\Http\Controllers\Auth\AdminLoginController@logout') ->name('dashboard.logout');

Route::group(['prefix' => '/'       ,       'middleware'    => 'auth:admin'], function ()
{
    Route::get('/home'              ,       function ()
    {
        return view('dashboard.index');
    })->name('dashboard.index');

    Route::group(['prefix'               =>      '/categories'   ,      'middleware' => 'auth:admin'], function ()
    {
        Route::get('/'                   ,       'CategoryController@index')         ->name('categories.index');
        Route::post('changeStatus'       ,       'CategoryController@changeStatus')  ->name('categories.changeStatus');
        Route::post('store'              ,       'CategoryController@store')         ->name('categories.store');
        Route::get('destroy/{id}'        ,       'CategoryController@destroy')       ->name('categories.destroy');
        Route::post('update/{id}'        ,       'CategoryController@update')        ->name('categories.update');
    });

    Route::group(['prefix'               =>      '/courses'   ,      'middleware' => 'auth:admin'], function ()
    {
        Route::get('/'                   ,       'CoursesController@index')          ->name('courses.index');
        Route::post('changeStatus'       ,       'CoursesController@changeStatus')   ->name('courses.changeStatus');
        Route::post('store'              ,       'CoursesController@store')          ->name('courses.store');
        Route::get('destroy/{id}'        ,       'CoursesController@destroy')        ->name('courses.destroy');
        Route::post('update/{id}'        ,       'CoursesController@update')         ->name('courses.update');
    });

    //Route::resources('categories'        ,     'CategoryController');
});


Route::any('/{slug?}'   ,       function ()
{
    return redirect('/dashboard/home');
});

