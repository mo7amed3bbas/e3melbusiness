<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\courseResource;
use App\Http\Resources\categoryResource;
use App\Models\course;
use App\Models\category;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/'          ], function ()
{
    Route::get('/courses'       , function () {
        return courseResource::collection(course::all());
    });
    Route::get('/categories'    , function () {
        return categoryResource::collection(category::all());
    });

});

