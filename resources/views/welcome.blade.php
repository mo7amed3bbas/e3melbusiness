<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mohamed Abbas</title>
    <link rel="shortcut icon" sizes="1024x1024" href="{{ asset('images/e3melbusiness.PNG') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }} ">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">

    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    @if (\App::isLocale('ar'))
        <link rel="stylesheet" href="{{asset('css/app_ar.css')}}">
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>


<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="{{ asset('images/e3melbusiness.PNG') }}" alt="AdminLTELogo" height="60" width="60">
        </div>

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light w-100 mr-0">
            <!-- Left navbar links -->
            <ul class="navbar-nav w-50">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                        class="fas fa-bars"></i>
                    </a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ url('/') }}" class="nav-link">{{ __('dashboard.Home')}}</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ url('/dashboard') }}" class="nav-link">{{ __('dashboard.Dashboard')}}</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto w-100 ml-10">


                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown w-100">
                    <input type="text" name="search" id="search_input" class="form-control w-100" placeholder="Search by Name / Category"
                    onKeyUp="search()" onchange="search()">
                </li>
                <!-- Notifications Dropdown Menu -->
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper mr-0">
            <!-- Content Header (Page header) -->

            <!-- Main content -->
            <section class="content">
               <div class="row">

                   <div class="col-2 sidebar p-3">
                    category_id
                    <hr>
                    <div class="category-div" id="category-div">
                        @php
                            $category = null;
                        @endphp
                        @foreach ($categories as $category)
                            <span class="category-span" onClick="document.getElementById('category_id').value={{ $category->id }}">{{ $category->name }} <small>( {{ $category->countCourses }} )</small></span>
                        @endforeach
                        <input type="hidden" name="category_id" id="category_id" value="0">
                        <input type="hidden" name="last_category_id" id="last_category_id" value="{{@$category->id}}">
                    </div>
                    <span class="category-span green pr-3" onclick="loadMoreCategs()">load more <i class="fa fa-plus"></i></span>
                    <br>
                    <b>Course Rating</b>
                    <hr>
                    <input type="radio" name="votes" id="votes1" onClick="document.getElementById('votes').value=4.5">
                    <label for="votes1">
                        <div class="rating"> <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <span class="d-inline-block average-rating">(4.5)</span>
                        </div>
                    </label>
                    <br>

                    <input type="radio" name="votes" id="votes2" onClick="document.getElementById('votes').value=4">
                    <label for="votes2">
                        <div class="rating"> <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <span class="d-inline-block average-rating">(4)</span>
                        </div>
                    </label>
                    <br>

                    <input type="radio" name="votes" id="votes3"  onClick="document.getElementById('votes').value=3.4">
                    <label for="votes3">
                        <div class="rating"> <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <i class="fas fa-star filled"></i>
                            <span class="d-inline-block average-rating">(3.4)</span>
                        </div>
                    </label>
                    <br>
                    <input type="hidden" name="votes" id="votes" value="" onchange="search()">


                    <b>level</b>
                    <hr>
                    <input type="checkBox" name="beginner" id="beginner"  onClick="document.getElementById('level').value='beginner'">
                    <label for="beginner">beginner</label>
                    <br>

                    <input type="checkBox" name="Intermediate" id="Intermediate"  onClick="document.getElementById('level').value='Intermediate'">
                    <label for="Intermediate">Intermediate</label>
                    <br>

                    <input type="checkBox" name="high" id="high"  onClick="document.getElementById('level').value='high'">
                    <label for="high">high</label>

                    <br>
                    <input type="hidden" name="level" id="level" value="" onchange="search()">

                   </div>


                   <div class="col-10 p-3">
                       <div class="row" id="courses-row">


                           @foreach ($courses as $course)
                           <div class="col-md-3">
                               <div class="card">
                                   <div class="card-body">
                                       <div class="row form-row">
                                           <div class="col-md-12">
                                               <div class="form-group">
                                                   <div class="change-avatar">
                                                       <div class="profile-img">
                                                           <img src="{{ asset('images/e3melbusiness.PNG') }}" alt="User Image" class="w-100 h-50" id="profileImg_1" >
                                                           <p>
                                                               <b class="green"> {{ $course->name }} </b><br>
                                                                <i class="fa fa-user"></i> Test User
                                                                {{ \Illuminate\Support\Str::limit($course->description, 150, '...') }}
                                                                <br>
                                                                <div class="rating"> <i class="fas fa-star filled"></i>
                                                                    <i class="fas fa-star filled"></i>
                                                                    <i class="fas fa-star filled"></i>
                                                                    <i class="fas fa-star filled"></i>
                                                                    <i class="fas fa-star filled"></i>
                                                                    <span class="d-inline-block average-rating">({{ $course->votes }})</span>
                                                                </div>
                                                            </p>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>

                                       </div>
                                   </div>
                               </div>
                           </div>
                           @endforeach
                       </div>
                   </div>

               </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.2.0-rc
            </div>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script>
        function search()
        {
            var pastHtml    =   $('#courses-row').html();
            var text        =   $('#search_input').val();
            var category_id =   $('#category_id').val();
            var level       =   $('#level').val();
            var votes       =   $('#votes').val();
            $('#courses-row').html('••••');
            $.ajax({
                type: "post",
                url: "{{ route('home.search')}}",
                data: {
                    text            :   text,
                    category_id     :   category_id,
                    level           :   level,
                    votes           :   votes,
                    _token          :   '{{csrf_token()}}'
                },
                success: function(data)
                {
                    $('#courses-row').html('');
                    for (let index = 0; index < data.length; index++)
                    {
                        var html        =   '';
                        const element   = data[index];
                        var title       = element.title;
                        var description = element.description;
                        console.log('description' , description);
                        html            = '<div class="col-md-3"> <div class="card"> <div class="card-body"> <div class="row form-row"> <div class="col-md-12"> <div class="form-group"> <div class="change-avatar"> <div class="profile-img"> <img src="{{ asset('images/e3melbusiness.PNG') }}" alt="User Image" class="w-100 h-50" id="profileImg_1" > <p> <b class="green"> {{ $course->name }} </b><br> <i class="fa fa-user"></i> Test User <br >';
                        html            = html + description + '<br> <div class="rating"> <i class="fas fa-star filled"></i> <i class="fas fa-star filled"></i> <i class="fas fa-star filled"></i> <i class="fas fa-star filled"></i> <i class="fas fa-star filled"></i> <span class="d-inline-block average-rating">({{ $course->votes }})</span>';
                        html            = html + '</div> </p> </div> </div> </div> </div> </div> </div> </div> </div>';
                        $('#courses-row').append(html);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#courses-row').append(pastHtml);
                    swal({
                        title:"e3melbusiness",
                        text:"error retreiving data",
                        type: "error"
                    });
                }
            });
        }

        function loadMoreCategs()
        {
            $.ajax({
                type: "get",
                url: "{{ url('categories/loadmore')}}"+'/'+last_category_id,
                success: function(data)
                {
                    for (let index = 0; index < data.length; index++)
                    {
                        const element       = data[index];
                        var id              = element.id;
                        var name            = element.name;
                        var countCourses    = element.countCourses;

                        var html            =   '<span class="category-span" onClick="document.getElementById("category_id").value='+ id +'">'+ name +' <small>( '+ countCourses +' )</small></span>';
                        $('#category-div').append(html);
                        $('#last_category_id').val(id);

                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    swal({
                        title:"e3melbusiness",
                        text:"error retreiving data",
                        type: "error"
                    });
                }
            });
        }

    </script>
</body>

</html>
