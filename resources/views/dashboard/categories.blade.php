@extends('dashboard.layouts.master')

@section('content')

    <div class="card-header">
        <h3 class="card-title">الأقسام</h3>
        <div class="card-tools">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#editCategModa" onclick="categModal()">
                    <i class="fas fa-plus">
                    </i>
                    إضافة جديد
                </button>
        </div>
    </div>

    <!-------------------------- message -------------------------->

    @if ($errors->has('message'))
        <p class="msg text-center" onclick="this.style.display='none';" style=" color:white;  @if ($errors->first('success') == 0 || $errors->first('success') == false)
        background-color:brown; @else background-color: #19c600; @endif ">
            {{ $errors->first('message') }}
        </p>
    @else
        @if (count($errors))
            <p class="msg text-center" onclick="this.style.display='none';">
                <span class="help-block text-center">
                    <strong>{{ __($errors->first()) }}</strong>
                </span>
            </p>
        @endif
    @endif
    <!-------------------------- message -------------------------->

    <div class="card-body p-0 " id="tooltip2">
        <table class="table table-striped projects" id="tooltip">
            <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>

                    <th style="width: 20%">
                        إسم القسم
                    </th>
                    <th style="width: 20%">
                        عدد الكورسات
                    </th>
                    <th style="width: 20%">
                        الحالة
                    </th>

                    <th style="width: 25%;" class="text-right">
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr id="row_{{$category->id }}">
                        <td>
                            {!! $category->id !!}
                        </td>

                        <td>
                            <span id="name_{{ $category->id }}">{!! $category->name !!}</span>

                        </td>
                        <td>
                            {!! $category->countCourses !!}
                        </td>
                        <td>
                            <i class="fa fa-{{ ($category->status == 'Active') ? 'check' : 'times' }} custom-fa i_{{$category->id}}"></i>
                        </td>

                        <td class="project-actions text-right">
                            <button class="btn btn-primary btn-sm btn_{{$category->id}}" onClick="changeStatus({{$category->id}} , '{{ $category->status }}')" >
                                <i class="fas fa-{{ ($category->status != 'Active') ? 'check' : 'times' }} ib_{{$category->id}}"> </i>
                                <span class="span_{{$category->id}}">{{ ($category->status != 'Active') ? 'تفعيل' : 'تعطيل' }}</span>
                            </button>

                            <button class="btn btn-info btn-sm" data-toggle="modal"
                                data-target="#editCategModa" onclick="categModal({{ $category->id }})">
                                <i class="fas fa-pencil-alt">
                                </i>
                                تعديل
                            </button>

                            <button class="btn btn-danger btn-sm" onclick="deleteCategory({!! $category->id !!})">
                                <i class="fas fa-trash"></i>
                                حذف
                            </button>

                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>

    </div>

    <div class="card-header mt-3 w-100 text-bold">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8 text-center">
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade w-100" id="editCategModa" role="dialog">
        <div class="modal-dialog w-100">
            <!-- Modal content-->
            <div class="modal-content  text-center" style=" box-shadow: 1px 2px 2px gray; width:900px;">
                <div class="modal-header text-center">
                    <h4 class="modal-title ">تعديل القسم</h4>
                    <div class="text-right">
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body text-center text-left" style="padding: 0; width: 100%;">
                    <form method="post" action="#" id="category_form"
                        enctype="multipart/form-data">
                        @csrf
                        <br />
                        <input type="hidden" name="category_id" id="category_id" value="">
                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>الإسم</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <input type="text" class="form-control titlef " name="name" id="name" required
                                    value="" />
                            </div>
                        </div>
                        <div class="col-12 text-center mb-5 mt-4">
                            <input type="submit" class="btn btn-primary btn-save w-25 pl-5 pr-5"
                                value="حفظ" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <script>
        function changeStatus( category_id , curr_status)
        {
            console.log(category_id , curr_status);
            var msg     = 'هل تريد تفعيل القسم';
            var kw      = 'تعطيل';
            var past_fa = 'fa fa-check';
            var new_fa  = 'fa fa-times';
            var new_stat= 'Active';
            if(curr_status == 'Active')
            {
                msg     = 'هل تريد إلغاء تفعيل القسم';
                kw      = 'تفعيل';
                past_fa = 'fa fa-times';
                new_fa  = 'fa fa-check';
                new_stat= 'inActive';
            }
            swal({
                title: "تغيير الحالة",
                text: msg,
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                confirmButtonText: "{{ __(' ✔ نعم') }}",
                cancelButtonText: "{{ __(' ✖ إلغاء') }}",
                confirmButtonClass:'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
            },
            function() {
                setTimeout(function() {
                    $.post("{{ route('categories.changeStatus') }}",
                    {
                        _token: '{{ csrf_token() }}',
                        category_id: category_id,
                    },
                    function(data, status) {
                        if(data == true)
                        {
                            swal({
                                title:"حالة القسم",
                                text:"تم تغيير الحالة بنجاح",
                                type: "success"
                                },
                                function() {
                                    $('.btn_'+category_id).attr('onclick' , "changeStatus(1 , '"+new_stat+"')");
                                    $('.span_'+category_id).html(kw);
                                    $('.ib_'+category_id).removeClass(past_fa).addClass(new_fa);
                                    $('.i_'+category_id).removeClass(new_fa).addClass(past_fa);

                                }
                            );
                        }
                        else
                        {
                            swal({
                                title:"حالة القسم",
                                text:"عفواَ ، لم تتم العملية",
                                type: "fail"
                                },
                                function() {
                                //location.reload();

                                }
                            );
                        }
                    });
                }, 10);
            });
        }

        function deleteCategory( category_id)
        {
            swal({
                title: "حذف القسم",
                text: "هل تريد حذف القسم؟",
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                confirmButtonText: "{{ __(' ✔ نعم') }}",
                cancelButtonText: "{{ __(' ✖ إلغاء') }}",
                confirmButtonClass:'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
            },
            function() {
                setTimeout(function() {

                    $.get("{{ url('dashboard/categories/destroy') }}"+'/'+category_id,
                    function(data, status) {
                        if(data == true)
                        {
                            swal({
                                title:"حذف القسم",
                                text:"تم الحذف بنجاح",
                                type: "success"
                                },
                                function() {
                                    $('#row_'+category_id).remove();
                                }
                            );
                        }
                        else
                        {
                            swal({
                                title:"حذف القسم",
                                text:"عفواَ ، لم تتم العملية",
                                type: "fail"
                                },
                                function() {
                                //location.reload();

                                }
                            );
                        }
                    });
                }, 10);
            });
        }

        function categModal(typ = 0)
        {
            $('#name').attr('placeholder' , 'الإسم');
            if(typ == 0)
            {
                $('#category_form').attr('method' , 'post');
                $('#category_form').attr('action' , "{{ route('categories.store') }}");
                $('#category_id').val(0);
                $('#name').val('');
            }
            else
            {
                $('#category_form').attr('method' , 'post');
                $('#category_form').attr('action' , "{{ route('categories.update',0) }}"+typ);
                $('#category_id').val(typ);
                $('#name').val($('#name_'+typ).html());
            }

        }
    </script>
@endsection

