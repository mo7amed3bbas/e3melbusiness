@extends('dashboard.layouts.master')

@section('content')

    <div class="card-header">
        <h3 class="card-title">الكورسات</h3>
        <div class="card-tools">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#editCategModa" onclick="categModal()">
                    <i class="fas fa-plus">
                    </i>
                    إضافة جديد
                </button>
        </div>
    </div>

    <!-------------------------- message -------------------------->

    @if ($errors->has('message'))
        <p class="msg text-center p-2" onclick="this.style.display='none';" style=" color:white;  @if ($errors->first('success') == 0 || $errors->first('success') == false)
        background-color:brown; @else background-color: #19c600; @endif ">
            {{ $errors->first('message') }}
        </p>
    @else
        @if (count($errors))
            <p class="msg text-center" onclick="this.style.display='none';">
                <span class="help-block text-center">
                    <strong>{{ __($errors->first()) }}</strong>
                </span>
            </p>
        @endif
    @endif
    <!-------------------------- message -------------------------->

    <div class="card-body p-0" id="tooltip2" style="display:inline-block;">
        <table class="table table-striped projects" id="tooltip">
            <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>

                    <th style="width: 15%">
                        إسم الكورس
                    </th>
                    <th style="width: 20%">
                        الوصف
                    </th>
                    <th style="width: 10%">
                        المستوي
                    </th>
                    <th style="width: 10%">
                        عدد الساعات
                    </th>
                    <th style="width: 10%">
                        التصويتات
                    </th>
                    <th style="width: 10%">
                        الحالة
                    </th>

                    <th style="width: 20%;" class="text-right">
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($courses as $course)
                    <tr id="row_{{$course->id }}">
                        <td>
                            {!! $course->id !!}
                        </td>

                        <td>
                            <span id="name_{{ $course->id }}">{!! $course->name !!}</span>
                        </td>
                        <td>
                            <span id="desc_{{ $course->id }}">{!! $course->description !!}</span>
                        </td>
                        <td>
                            <span id="level_{{ $course->id }}">{!! $course->level !!}</span>
                        </td>
                        <td>
                            <span id="hours_{{ $course->id }}">{!! $course->hours !!}</span>
                        </td>
                        <td>
                            <span id="categ_id_{{ $course->id }}" class="d-none">{!! @$course->category_id !!}</span>
                            {!! @$course->category->name !!}
                        </td>
                        <td>
                            <span id="status_{{ $course->id }}" class="d-none">{!! @$course->status !!}</span>
                            <i class="fa fa-{{ ($course->status == 'Active') ? 'check' : 'times' }} custom-fa i_{{$course->id}}"></i>
                        </td>

                        <td class="project-actions text-right">
                            <button class="btn btn-primary btn-sm btn_{{$course->id}}" onClick="changeStatus({{$course->id}} , '{{ $course->status }}')" >
                                <i class="fas fa-{{ ($course->status != 'Active') ? 'check' : 'times' }} ib_{{$course->id}}"> </i>
                                <span class="span_{{$course->id}}">{{ ($course->status != 'Active') ? 'تفعيل' : 'تعطيل' }}</span>
                            </button>

                            <button class="btn btn-info btn-sm" data-toggle="modal"
                                data-target="#editCategModa" onclick="categModal({{ $course->id }})">
                                <i class="fas fa-pencil-alt">
                                </i>
                                تعديل
                            </button>

                            <button class="btn btn-danger btn-sm" onclick="deletecourse({!! $course->id !!})">
                                <i class="fas fa-trash"></i>
                                حذف
                            </button>

                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>

    </div>

    <div class="card-header mt-3 w-100 text-bold">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8 text-center">
                    {{ $courses->links() }}
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade w-100" id="editCategModa" role="dialog">
        <div class="modal-dialog w-100">
            <!-- Modal content-->
            <div class="modal-content  text-center" style=" box-shadow: 1px 2px 2px gray; width:900px;">
                <div class="modal-header text-center">
                    <h4 class="modal-title ">تعديل القسم</h4>
                    <div class="text-right">
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body text-center text-left" style="padding: 0; width: 100%;">
                    <form method="post" action="#" id="course_form"
                        enctype="multipart/form-data">
                        @csrf
                        <br />
                        <input type="hidden" name="course_id" id="course_id" value="">

                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>الإسم</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <input type="text" class="form-control titlef " name="name" id="name" required
                                    value="" />
                            </div>
                        </div>

                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>الوصف</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <textarea name="description" class="form-control titlef " id="description"  rows="5" value=""></textarea>
                            </div>
                        </div>


                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>المستوي</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <select class="form-control titlef " name="level" id="level">
                                    <option value="beginner">beginner</option>
                                    <option value="intermediate">intermediate</option>
                                    <option value="high">high</option>
                                </select>
                            </div>
                        </div>


                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>عدد الساعات</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <input type="number" min="1" class="form-control titlef " name="hours" id="hours" required
                                    value="" />
                            </div>
                        </div>


                        <div class="row det-row">
                            <div class="col-md-3 text-center">
                                <b>القسم</b>
                            </div>
                            <div class="col-md-6 text-left">
                                <select class="form-control titlef " name="category_id" id="categ_id">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-12 text-center mb-5 mt-4">
                            <input type="submit" class="btn btn-primary btn-save w-25 pl-5 pr-5"
                                value="حفظ" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <script>
        function changeStatus( course_id , curr_status)
        {
            console.log(course_id , curr_status);
            var msg     = 'هل تريد تفعيل القسم';
            var kw      = 'تعطيل';
            var past_fa = 'fa fa-check';
            var new_fa  = 'fa fa-times';
            var new_stat= 'Active';
            if(curr_status == 'Active')
            {
                msg     = 'هل تريد إلغاء تفعيل القسم';
                kw      = 'تفعيل';
                past_fa = 'fa fa-times';
                new_fa  = 'fa fa-check';
                new_stat= 'inActive';
            }
            swal({
                title: "تغيير الحالة",
                text: msg,
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                confirmButtonText: "{{ __(' ✔ نعم') }}",
                cancelButtonText: "{{ __(' ✖ إلغاء') }}",
                confirmButtonClass:'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
            },
            function() {
                setTimeout(function() {
                    $.post("{{ route('courses.changeStatus') }}",
                    {
                        _token: '{{ csrf_token() }}',
                        id: course_id,
                    },
                    function(data, status) {
                        if(data == true)
                        {
                            swal({
                                title:"حالة القسم",
                                text:"تم تغيير الحالة بنجاح",
                                type: "success"
                                },
                                function() {
                                    $('.btn_'+course_id).attr('onclick' , "changeStatus(1 , '"+new_stat+"')");
                                    $('.span_'+course_id).html(kw);
                                    $('.ib_'+course_id).removeClass(past_fa).addClass(new_fa);
                                    $('.i_'+course_id).removeClass(new_fa).addClass(past_fa);

                                }
                            );
                        }
                        else
                        {
                            swal({
                                title:"حالة القسم",
                                text:"عفواَ ، لم تتم العملية",
                                type: "fail"
                                },
                                function() {
                                //location.reload();

                                }
                            );
                        }
                    });
                }, 10);
            });
        }

        function deletecourse( course_id)
        {
            swal({
                title: "حذف القسم",
                text: "هل تريد حذف القسم؟",
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                confirmButtonText: "{{ __(' ✔ نعم') }}",
                cancelButtonText: "{{ __(' ✖ إلغاء') }}",
                confirmButtonClass:'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
            },
            function() {
                setTimeout(function() {

                    $.get("{{ url('dashboard/courses/destroy') }}"+'/'+course_id,
                    function(data, status) {
                        if(data == true)
                        {
                            swal({
                                title:"حذف القسم",
                                text:"تم الحذف بنجاح",
                                type: "success"
                                },
                                function() {
                                    $('#row_'+course_id).remove();
                                }
                            );
                        }
                        else
                        {
                            swal({
                                title:"حذف القسم",
                                text:"عفواَ ، لم تتم العملية",
                                type: "fail"
                                },
                                function() {
                                //location.reload();

                                }
                            );
                        }
                    });
                }, 10);
            });
        }

        function categModal(typ = 0)
        {
            $('#name').attr('placeholder' , 'الإسم');
            if(typ == 0)
            {
                $('#course_form').attr('method' , 'post');
                $('#course_form').attr('action' , "{{ route('courses.store') }}");
                $('#course_id').val(0);
                $('#name').val('');
            }
            else
            {

                $('#course_form').attr('method' , 'post');
                $('#course_form').attr('action' , "{{ route('courses.update',0) }}"+typ);
                $('#course_id').val(typ);
                $('#name').val($('#name_'+typ).html());
                $('#description').val($('#desc_'+typ).html());
                $('#hours').val($('#hours_'+typ).html());
                $('#level').val($('#level_'+typ).html()).trigger('change');
                $('#categ_id').val($('#categ_id_'+typ).html()).trigger('change');
            }

        }
    </script>
@endsection

