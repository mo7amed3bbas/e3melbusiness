<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class courseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => @$this->id,
            'name'          => @$this->name,
            'description'   => @$this->description,
            'level'         => @$this->level,
            'votes'         => @$this->votes,
            'hours'         => @$this->hours,
            'category'      => @$this->category->name,
            'status'        => @$this->status,
            'created_at'    => @$this->created_at,
            'updated_at'    => @$this->updated_at,
        ];
    }
}
