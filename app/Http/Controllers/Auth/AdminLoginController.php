<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Illuminate\Http\Request;
use Auth;
class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        return view('auth.adminLogin');
    }

    public function SignIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => 'required|email',
            'password'      => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors(['success'=>0,'message'=> 'These credentials do not match our records.']);
        }
        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=> $request->password], $request->remember)){
            return redirect(RouteServiceProvider::DashboardHOME)->withErrors(['success'=>1,'message'=> 'You are logged in Successfully']);
        }
        return back()->withErrors(['success'=>0,'message'=> 'These credentials do not match our records.']);
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/dashboard/login');
    }
}
