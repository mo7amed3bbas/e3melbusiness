<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\course;
use App\Models\category;
class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses    = course::withoutGlobalScope('active')->paginate(20);
        $categories = category::get(['id' , 'name']);
        return view('dashboard.courses' , compact('courses' , 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        category::create($request->except(['_token' , 'course_id']));
        return back()->withErrors(['success'=>1 , 'message'=> 'تمت الإضافة بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        course::withoutGlobalScope('active')->where('id' , $id)->update($request->except(['_token' , 'course_id']));
        return back()->withErrors(['success'=>1 , 'message'=> 'تم التعديل بنجاح']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course               =   course::withoutGlobalScope('active')->find($id);
        if($course)
        {
            if($course->delete())
            {
                return true;
            }
        }
        return false;
    }

    public function changeStatus(Request $request)
    {
        $category               =   course::withoutGlobalScope('active')->find($request->id);
        if($category)
        {
            $category->status   =   ($category->status == 'Active') ? 'inActive' : 'Active';
            if($category->save())
            {
                return true;
            }
        }
        return false;
    }

}
