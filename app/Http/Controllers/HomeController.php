<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\course;
use \App\Models\category;
use App\Http\Resources\courseResource;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $courses    = course::take(12)->get();
        $categories = category::take(12)->get(['id','name' ,'countCourses']);
        return view('welcome',compact('courses' , 'categories'));
    }

    public function search(Request $request)
    {
        $text           =   $request->text;
        $category_id    =   $request->category_id;
        $level          =   $request->level;
        $votes          =   $request->votes;
        $hours          =   $request->hours;

        $courses = course::where('id' , '>' , 0);
        if($text)
        {
            $courses = $courses->where('name'       , 'LIKE' , "%$text%")
            ->orWhere('description'     , 'LIKE'    , "%$text%")
            ->orWhereHas('category'     , function($query) use($text){
                $query->where('name'    , 'LIKE'    , "%$text%");
            });
        }
        if($category_id)
        {
            $courses = $courses->where('category_id' , $category_id);
        }
        if($level)
        {
            $courses = $courses->where('level' , $level);
        }
        if($votes)
        {
            $courses = $courses->where('votes' , $votes);
        }
        if($hours)
        {
            $courses = $courses->where('hours' , $hours);
        }
        return response()->json($courses->limit(15)->get());
    }

    public function loadMoreCategories($id)
    {
        $categories =  category::where('id' , '>' , $id)->take(12)->get(['id','name' ,'countCourses']);
        return response()->json($categories);
    }
}
